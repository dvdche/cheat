# Git tricks : 

## Look for deleted file :

```bash
 git log --all --full-history -- <relative path of file>
```
